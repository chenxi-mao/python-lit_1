Name:          python-lit
Version:       15.0.6
Release:       1
BuildArch:     noarch
License:       NCSA
Summary:       A tool to execute the llvm test suite
URL:           https://pypi.python.org/pypi/lit
Source0:       https://files.pythonhosted.org/packages/3c/e4/aa93b44e5983672069f608e96624eab10890d2361fe0b18546d605bdcb1a/lit-15.0.6.tar.gz
BuildRequires: python3-devel python3-setuptools

%description
python-lit is a tool used by LLVM to execute its test suite.

%package -n python3-lit
Summary: LLVM lit test runner for Python 3
Requires: python3-setuptools

%description -n python3-lit
python-lit is a tool used by LLVM to execute its test suite.

%prep
%autosetup -n lit-%{version}

%build
%py3_build

%install
%py3_install
sed -i -e '1{\@^#!/usr/bin/env python@d}' %{buildroot}%{python3_sitelib}/lit/*.py

%files -n python3-lit
%license LICENSE.TXT
%doc README.txt
%{python3_sitelib}/*
%{_bindir}/lit

%changelog
* Fri Dec 23 2022 Chenxi Mao <chenxi.mao@suse.com> - 15.0.6-1
- Upgrade to version 15.0.6

* Tue Jun 21 2022 houyingchao <houyingchao@h-partners.com> - 14.0.3-1
- Upgrade to version 14.0.3

* Mon Oct 26 2020 Ge Wang<wangge20@huawei.com> - 0.7.0-4
- remove python2

* Sat Nov 30 2019 gulining<gulining1@huawei.com> - 0.7.0-3
- Pakcage init
